﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TP1_CreditImmobilier.Models;

namespace TP1_CreditImmobilier.Controllers
{
    public class SimulationController : Controller
    {
        //
        // GET: /Simulation/


        private void majResultatSimulation(ResultatSimulationModel resultatSimulation)
        {
            for (int ech = 0; ech < resultatSimulation.simulation.dureePret; ech++)
            {
                LigneTableauAmortissementModel la = new LigneTableauAmortissementModel();
                if (ech == 0)
                {
                    la.capitalRestant = resultatSimulation.simulation.capitalEmprunte;
                }
                else
                {
                    LigneTableauAmortissementModel laPrec = resultatSimulation.listeLignesAmortissement[ech - 1];
                    la.capitalRestant = Math.Round(laPrec.capitalRestant - laPrec.amortissement, SimulationModel.nbDecimal);
                }
                la.interet = Math.Round(la.capitalRestant * resultatSimulation.simulation.tauxInteretPeriodique, SimulationModel.nbDecimal);
                resultatSimulation.coutTotalInteret += la.interet;
                if (ech < resultatSimulation.simulation.dureePret - 1)
                {
                    la.amortissement = Math.Round(resultatSimulation.simulation.montantEcheances - la.interet, SimulationModel.nbDecimal);
                }
                else
                {
                    la.amortissement = la.capitalRestant;
                }
                la.frais = Math.Round(resultatSimulation.simulation.montantEcheancesAssurance, SimulationModel.nbDecimal);
                resultatSimulation.coutTotalAssurance += la.frais;
                la.montantReel = la.interet + la.amortissement + la.frais;
                resultatSimulation.coutTotalPret += la.montantReel;
                resultatSimulation.listeLignesAmortissement.Add(la);
            }
        }

        private int getPagesNumber()
        {
            var repo = new SimulationRepository(new Entities());
            return repo.All().Count() / 10;
        }


        private Simulation createSimulationToSimulationModel(ResultatSimulationModel s)
        {
            Simulation sim = new Simulation();
            sim.Id = s.simulation.id;
            sim.Capital = s.simulation.capitalEmprunte;
            sim.Date = s.simulation.date;
            sim.Duree = s.simulation.dureePret;
            //sim.NomClient = s.simulation.nomClient;
            sim.NbEcheancesParAn = s.simulation.nbEcheanceParAn;
            sim.MontantEcheance = s.simulation.montantEcheances;
            sim.TauxAssurance = s.simulation.tauxAssurance;
            sim.TauxInteretAnnuel = s.simulation.tauxInteretAnnuel;
            sim.TauxInteretPeriodique = s.simulation.tauxInteretPeriodique;
            sim.TauxPeriodiqueAssurance = s.simulation.tauxPeriodiqueAssurance;
            sim.MontantEcheanceAssurance = s.simulation.montantEcheancesAssurance;
            sim.CoutTotalAssurance = s.coutTotalAssurance;
            sim.CoutTotalInterets = s.coutTotalInteret;
            sim.CoutTotalPret = s.coutTotalPret;
            sim.UserId = s.simulation.UserId;
            return sim;
        }


        public ActionResult Index(int? pageNumber)
        {
            if (!pageNumber.HasValue)
            {
                pageNumber = 0;
            }
            ViewBag.Title = "Toutes les simulations";
            ViewBag.actionName = "Index";
            var entity  = new Entities();
            var repo = new SimulationRepository(entity);
            var  simulations = repo.All().Join(entity.UserProfiles, (s => s.UserId), (up => up.UserId), (s, up) => new SimulationModel {
                id = s.Id,
                nomClient = up.UserName,
                date = s.Date,
                capitalEmprunte = s.Capital,
                dureePret = s.Duree,
                nbEcheanceParAn = s.NbEcheancesParAn,
                montantEcheances = s.MontantEcheance,
                tauxInteretAnnuel = s.TauxInteretAnnuel,
                tauxAssurance = s.TauxAssurance,
                tauxInteretPeriodique = s.TauxInteretPeriodique,
                tauxPeriodiqueAssurance = s.TauxPeriodiqueAssurance,
                montantEcheancesAssurance = s.MontantEcheanceAssurance,
                UserId = s.UserId
            }).OrderBy(s => s.id);
            var repoUser = new UserRepository(new Entities());
            PaginatedList<SimulationModel> pageList = new PaginatedList<SimulationModel>(simulations, pageNumber.Value, 10);
            return View(pageList);
        }

        [Authorize]
        [HttpGet]
        public ActionResult MySimulations(int? pageNumber) {
            if (!pageNumber.HasValue)
            {
                pageNumber = 0;
            }
            ViewBag.Title = "Mes simulations";
            ViewBag.actionName = "MySimulations";

            var entity = new Entities();
            var repo = new SimulationRepository(entity);
            var simulations = repo.All()
                .Join(entity.UserProfiles, (s => s.UserId), (up => up.UserId), (s, up) => new SimulationModel
            {
                id = s.Id,
                nomClient = up.UserName,
                date = s.Date,
                capitalEmprunte = s.Capital,
                dureePret = s.Duree,
                nbEcheanceParAn = s.NbEcheancesParAn,
                montantEcheances = s.MontantEcheance,
                tauxInteretAnnuel = s.TauxInteretAnnuel,
                tauxAssurance = s.TauxAssurance,
                tauxInteretPeriodique = s.TauxInteretPeriodique,
                tauxPeriodiqueAssurance = s.TauxPeriodiqueAssurance,
                montantEcheancesAssurance = s.MontantEcheanceAssurance,
                UserId = s.UserId
            }).Where(s => s.nomClient == User.Identity.Name).OrderBy(s => s.id);
            PaginatedList<SimulationModel> pageList = new PaginatedList<SimulationModel>(simulations, pageNumber.Value, 10);
            return View("~/Views/Shared/Index.cshtml", pageList);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Create()
        {
            SimulationModel s = new SimulationModel();
            //s.nomClient = "Pierre de Perpignan";
            s.date = DateTime.Now;
            var entity = new Entities();
            var repo = new SimulationRepository(entity);
            var UserRepo = new UserRepository(entity);
            UserProfile userProfile = UserRepo.GetUserByUserName(User.Identity.Name).First();
            s.UserId = userProfile.UserId;
            s.nomClient = userProfile.UserName;
            Simulation lastSim = repo.All().OrderByDescending(simulation => simulation.Id).First();
            s.id = lastSim.Id + 1;
            return View(s);
        }

        [HttpPost]
        public ActionResult Create(SimulationModel s)
        {
            if (ModelState.IsValid)
            {
                ResultatSimulationModel resSimulationModel = new ResultatSimulationModel();
                resSimulationModel.simulation = s;
                majResultatSimulation(resSimulationModel);
                var repo = new SimulationRepository(new Entities());
                Simulation sim = createSimulationToSimulationModel(resSimulationModel);
                repo.Add(sim);
                repo.Save();  
                return View("~/Views/Shared/Resultat.cshtml", s);
            }
            else
            {
                return View(s);
            }
        }

       
        [HttpGet]
        public ActionResult Read(int id)
        {
            var entity = new Entities();
            var repo = new SimulationRepository(entity);
            SimulationModel sm = repo.getById(id).Join(entity.UserProfiles, (s => s.UserId), (up => up.UserId), (s, up) => new SimulationModel
            {
                id = s.Id,
                nomClient = up.UserName,
                date = s.Date,
                capitalEmprunte = s.Capital,
                dureePret = s.Duree,
                nbEcheanceParAn = s.NbEcheancesParAn,
                montantEcheances = s.MontantEcheance,
                tauxInteretAnnuel = s.TauxInteretAnnuel,
                tauxAssurance = s.TauxAssurance,
                tauxInteretPeriodique = s.TauxInteretPeriodique,
                tauxPeriodiqueAssurance = s.TauxPeriodiqueAssurance,
                montantEcheancesAssurance = s.MontantEcheanceAssurance,
                UserId = s.UserId
            }).First();
            return View("~/Views/Shared/Resultat.cshtml", sm);
        }


        [HttpGet]
        public ActionResult Edit(int id_request)
        {
            var entity = new Entities();
            var repo = new SimulationRepository(entity);
            SimulationModel sm = repo.getById(id_request).Join(entity.UserProfiles, (s => s.UserId), (up => up.UserId), (s, up) => new SimulationModel
            {
                id = s.Id,
                nomClient = up.UserName,
                date = s.Date,
                capitalEmprunte = s.Capital,
                dureePret = s.Duree,
                nbEcheanceParAn = s.NbEcheancesParAn,
                montantEcheances = s.MontantEcheance,
                tauxInteretAnnuel = s.TauxInteretAnnuel,
                tauxAssurance = s.TauxAssurance,
                tauxInteretPeriodique = s.TauxInteretPeriodique,
                tauxPeriodiqueAssurance = s.TauxPeriodiqueAssurance,
                montantEcheancesAssurance = s.MontantEcheanceAssurance,
                UserId = s.UserId
            }).First();
            if (sm.nomClient != User.Identity.Name)
            {
                return View("~/Views/Shared/InvalidClient.cshtml");
            }
            return View("~/Views/Shared/Edit.cshtml", sm);
        }

        [HttpPost]
        public ActionResult Edit(SimulationModel sm)
        {
            var repo = new SimulationRepository(new Entities());
            ResultatSimulationModel resSimulationModel = new ResultatSimulationModel();
            resSimulationModel.simulation = sm;
            majResultatSimulation(resSimulationModel);
            Simulation s = repo.getById(sm.id).First();
            repo.Delete(s);
            s = createSimulationToSimulationModel(resSimulationModel);
            repo.Add(s);
            repo.Save();
            return View("~/Views/Shared/Resultat.cshtml", sm);
        
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var repo = new SimulationRepository(new Entities());
            Simulation s = repo.getById(id).First();
            repo.Delete(s);
            repo.Save();
            return RedirectToAction("Index", "Simulation");
        }

        [HttpPost]
        public ActionResult Search(string query)
        {
            String[] results = query.Split(new Char[]{' ', ','});
            var entity = new Entities();
            var repo = new SimulationRepository(entity);
            var simulations = repo.Search(results)
                .Join(entity.UserProfiles, (s => s.UserId), (up => up.UserId), (s, up) => new SimulationModel
            {
                id = s.Id,
                nomClient = up.UserName,
                date = s.Date,
                capitalEmprunte = s.Capital,
                dureePret = s.Duree,
                nbEcheanceParAn = s.NbEcheancesParAn,
                montantEcheances = s.MontantEcheance,
                tauxInteretAnnuel = s.TauxInteretAnnuel,
                tauxAssurance = s.TauxAssurance,
                tauxInteretPeriodique = s.TauxInteretPeriodique,
                tauxPeriodiqueAssurance = s.TauxPeriodiqueAssurance,
                montantEcheancesAssurance = s.MontantEcheanceAssurance,
                UserId = s.UserId
            }).OrderBy(s => s.id).ToList();
            return PartialView("~/Views/Shared/_SimulationResults.cshtml", simulations);
        }
    }
}
