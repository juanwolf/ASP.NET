﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TP1_CreditImmobilier.Models;

namespace TP1_CreditImmobilier.Controllers
{
    public class SimulationController : Controller
    {
        //
        // GET: /Simulation/


        private void majResultatSimulation(ResultatSimulationModel resultatSimulation)
        {
            for (int ech = 0; ech < resultatSimulation.simulation.dureePret; ech++)
            {
                LigneTableauAmortissementModel la = new LigneTableauAmortissementModel();
                if (ech == 0)
                {
                    la.capitalRestant = resultatSimulation.simulation.capitalEmprunte;
                }
                else
                {
                    LigneTableauAmortissementModel laPrec = resultatSimulation.listeLignesAmortissement[ech - 1];
                    la.capitalRestant = Math.Round(laPrec.capitalRestant - laPrec.amortissement, SimulationModel.nbDecimal);
                }
                la.interet = Math.Round(la.capitalRestant * resultatSimulation.simulation.tauxInteretPeriodique, SimulationModel.nbDecimal);
                resultatSimulation.coutTotalInteret += la.interet;
                if (ech < resultatSimulation.simulation.dureePret - 1)
                {
                    la.amortissement = Math.Round(resultatSimulation.simulation.montantEcheances - la.interet, SimulationModel.nbDecimal);
                }
                else
                {
                    la.amortissement = la.capitalRestant;
                }
                la.frais = Math.Round(resultatSimulation.simulation.montantEcheancesAssurance, SimulationModel.nbDecimal);
                resultatSimulation.coutTotalAssurance += la.frais;
                la.montantReel = la.interet + la.amortissement + la.frais;
                resultatSimulation.coutTotalPret += la.montantReel;
                resultatSimulation.listeLignesAmortissement.Add(la);
            }
        }

        private int getPagesNumber()
        {
            var repo = new SimulationRepository(new Entities());
            return repo.All().Count() / 10;
        }


        private Simulation createSimulationToSimulationModel(ResultatSimulationModel s)
        {
            Simulation sim = new Simulation();
            sim.Id = s.simulation.id;
            sim.Capital = s.simulation.capitalEmprunte;
            sim.Date = s.simulation.date;
            sim.Duree = s.simulation.dureePret;
            sim.NomClient = s.simulation.nomClient;
            sim.NbEcheancesParAn = s.simulation.nbEcheanceParAn;
            sim.MontantEcheance = s.simulation.montantEcheances;
            sim.TauxAssurance = s.simulation.tauxAssurance;
            sim.TauxInteretAnnuel = s.simulation.tauxInteretAnnuel;
            sim.TauxInteretPeriodique = s.simulation.tauxInteretPeriodique;
            sim.TauxPeriodiqueAssurance = s.simulation.tauxPeriodiqueAssurance;
            sim.MontantEcheanceAssurance = s.simulation.montantEcheancesAssurance;
            sim.CoutTotalAssurance = s.coutTotalAssurance;
            sim.CoutTotalInterets = s.coutTotalInteret;
            sim.CoutTotalPret = s.coutTotalPret;
            return sim;
        }


        public ActionResult Index(int? pageNumber)
        {
            if (!pageNumber.HasValue)
            {
                pageNumber = 0;
            }
            var repo = new SimulationRepository(new Entities());
            var  simulations = repo.All().OrderBy(s => s.Id).Select(s => new SimulationModel {
                id = s.Id,
                nomClient = s.NomClient,
                date = s.Date,
                capitalEmprunte = s.Capital,
                dureePret = s.Duree,
                nbEcheanceParAn = s.NbEcheancesParAn,
                montantEcheances = s.MontantEcheance,
                tauxInteretAnnuel = s.TauxInteretAnnuel,
                tauxAssurance = s.TauxAssurance,
                tauxInteretPeriodique = s.TauxInteretPeriodique,
                tauxPeriodiqueAssurance = s.TauxPeriodiqueAssurance,
                montantEcheancesAssurance = s.MontantEcheanceAssurance
            });
            PaginatedList<SimulationModel> pageList = new PaginatedList<SimulationModel>(simulations, pageNumber.Value, 10);
            return View(pageList);
        }

        [HttpGet]
        public ActionResult Create()
        {
            SimulationModel s = new SimulationModel();
            s.nomClient = "Pierre de Perpignan";
            s.date = DateTime.Now;
            var repo = new SimulationRepository(new Entities());
            s.id = repo.All().Count();
            return View(s);
        }


        public ActionResult GetPage(int pageNumber)
        {

            var repo = new SimulationRepository(new Entities());
            int simulationsSkippedNumber = pageNumber * 10;
            ViewData["Simulations"] = repo.All().Select(s => new SimulationModel
            {
                id = s.Id,
                nomClient = s.NomClient,
                date = s.Date,
                capitalEmprunte = s.Capital,
                dureePret = s.Duree,
                nbEcheanceParAn = s.NbEcheancesParAn,
                montantEcheances = s.MontantEcheance,
                tauxInteretAnnuel = s.TauxInteretAnnuel,
                tauxAssurance = s.TauxAssurance,
                tauxInteretPeriodique = s.TauxInteretPeriodique,
                tauxPeriodiqueAssurance = s.TauxPeriodiqueAssurance,
                montantEcheancesAssurance = s.MontantEcheanceAssurance
            }).OrderBy(s => s.id).Skip(simulationsSkippedNumber).Take(10).ToList();
            ViewData["PagesNumber"] = getPagesNumber();
            return View("~/Views/Shared/Index.cshtml");
        }

        [HttpPost]
        public ActionResult Create(SimulationModel s)
        {
            if (ModelState.IsValid)
            {
                ResultatSimulationModel resSimulationModel = new ResultatSimulationModel();
                resSimulationModel.simulation = s;
                majResultatSimulation(resSimulationModel);
                var repo = new SimulationRepository(new Entities());
                Simulation sim = createSimulationToSimulationModel(resSimulationModel);
                repo.Add(sim);
                repo.Save();  
                return View("~/Views/Shared/Resultat.cshtml", s);
            }
            else
            {
                return View(s);
            }
        }

       
        [HttpGet]
        public ActionResult Read(int id)
        {
            var repo = new SimulationRepository(new Entities());
            SimulationModel sm = repo.getById(id).Select(s => new SimulationModel {
                id = s.Id,
                nomClient = s.NomClient,
                date = s.Date,
                capitalEmprunte = s.Capital,
                dureePret = s.Duree,
                nbEcheanceParAn = s.NbEcheancesParAn,
                montantEcheances = s.MontantEcheance,
                tauxInteretAnnuel = s.TauxInteretAnnuel,
                tauxAssurance = s.TauxAssurance,
                tauxInteretPeriodique = s.TauxInteretPeriodique,
                tauxPeriodiqueAssurance = s.TauxPeriodiqueAssurance,
                montantEcheancesAssurance = s.MontantEcheanceAssurance
            }).First(); 
            return View("~/Views/Shared/Resultat.cshtml", sm);
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            var repo = new SimulationRepository(new Entities());
            SimulationModel sm = repo.getById(id).Select(s => new SimulationModel
            {
                id = s.Id,
                nomClient = s.NomClient,
                date = s.Date,
                capitalEmprunte = s.Capital,
                dureePret = s.Duree,
                nbEcheanceParAn = s.NbEcheancesParAn,
                montantEcheances = s.MontantEcheance,
                tauxInteretAnnuel = s.TauxInteretAnnuel,
                tauxAssurance = s.TauxAssurance,
                tauxInteretPeriodique = s.TauxInteretPeriodique,
                tauxPeriodiqueAssurance = s.TauxPeriodiqueAssurance,
                montantEcheancesAssurance = s.MontantEcheanceAssurance
            }).First();
            return View("~/Views/Shared/Edit.cshtml", sm);
        }

        [HttpPost]
        public ActionResult Edit(SimulationModel sm)
        {
            var repo = new SimulationRepository(new Entities());
            ResultatSimulationModel resSimulationModel = new ResultatSimulationModel();
            resSimulationModel.simulation = sm;
            majResultatSimulation(resSimulationModel);
            Simulation s = repo.getById(sm.id).First();
            repo.Delete(s);
            s = createSimulationToSimulationModel(resSimulationModel);
            repo.Add(s);
            return View("~/Views/Shared/Resultat.cshtml", sm);
        
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var repo = new SimulationRepository(new Entities());
            Simulation s = repo.getById(id).First();
            repo.Delete(s);
            repo.Save();
            return RedirectToAction("Index", "Simulation");
        }
    }
}
