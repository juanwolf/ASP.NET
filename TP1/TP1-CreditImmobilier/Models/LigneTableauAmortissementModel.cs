﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TP1_CreditImmobilier.Models
{
    public class LigneTableauAmortissementModel
    {
        [DisplayName("Capital restant")]
        public double capitalRestant{
            get;
            set;
        }

        [DisplayName("Intérêt")]
        public double interet
        {
            get;
            set;
        }

        [DisplayName("L'amortissement")]
        public double amortissement
        {
            get;
            set;
        }

        [DisplayName("Les frais")]
        public double frais
        {
            get;
            set;
        }

        [DisplayName("Le montant réel")]
        public double montantReel
        {
            get;
            set;
        }

    }
}