﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace TP1_CreditImmobilier.Models
{
    public class SimulationRepository
    {

        private Entities context;

        public SimulationRepository(Entities e)
        {
            context = e;
        }

        public IQueryable<Simulation> All() {
            return  context.Simulations;
        }

        public IQueryable<Simulation> getById(int id)
        {
           return  context.Simulations.Where(s => s.Id == id);
        }

        public IQueryable<Simulation> AllByUser(String user)
        {
            IQueryable<Simulation> simulations =
                context.Simulations.Where(s => s.NomClient == user);
            return simulations;
        }

        public IQueryable<Simulation> AllByUserAboveToTreshold(String user, int capital) 
        {
            IQueryable<Simulation> simulations =
                context.Simulations.Where(s => s.NomClient == user);
            IQueryable<Simulation> simulationsAboveToTreshold = simulations.Where(s => s.Capital > capital);
            return simulationsAboveToTreshold;
        }

        public void Add(Simulation s)
        {
            context.Simulations.Add(s);
        }

        public void Delete(Simulation s)
        {
            var lignes = s.SimulationsLignes.ToList();
            foreach (var l in lignes)
            {
                context.SimulationsLignes.Remove(l);
            }
            context.Simulations.Remove(s);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}