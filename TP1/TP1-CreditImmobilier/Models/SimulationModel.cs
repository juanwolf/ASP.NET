﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TP1_CreditImmobilier.Models
{
    public class SimulationModel
    {

        public const int nbDecimal = 2;

        [DisplayName("Nom du client")]
        public String nomClient {
            get; set;
        }

        [DisplayName("Date du prêt")]
        public DateTime date
        {
            get;
            set;
        }

        [DisplayName("Capital emprunté")]
        [Required(ErrorMessage="Capital emprunté est requis")]
        public double capitalEmprunte
        {
            get;
            set;
        }

        [DisplayName("Durée du prêt")]
        [Required(ErrorMessage="La durée du prêt est requise")]
        public int dureePret
        {
            get;
            set;
        }

        [DisplayName("Nombre d'échéances par an")]
        [Required(ErrorMessage="Le nombre d'échance est requis")]
        public int nbEcheanceParAn
        {
            get;
            set;
        }

        [DisplayName("Montant des échances")]
        [Required(ErrorMessage="Le montant des échéances est requis")]
        public double montantEcheances
        {
            get;
            set;
        }

        [DisplayName("Taux d'intérêt annuel")]
        [Required(ErrorMessage="Le taux d'intéret annuel est requis")]
        public double tauxInteretAnnuel
        {
            get;
            set;
        }

        [DisplayName("Taux d'assurance")]
        [Required(ErrorMessage="Le taux de l'assurrance est requis")]
        public double tauxAssurance
        {
            get;
            set;
        }

        [DisplayName("Taux d'intérêt périodique")]
        public double tauxInteretPeriodique
        {
            get;
            set;
        }

        [DisplayName("Taux périodique d'assurance")]
        public double tauxPeriodiqueAssurance
        {
            get;
            set;
        }

        [DisplayName("Montant des échéances d'assurance")]
        public double montantEcheancesAssurance
        {
            get;
            set;
        }

        public List<SelectListItem> getNombreEcheance
        {
            get
            {
                return new List<SelectListItem>() {
                    new SelectListItem{Text="1", Value="1"},
                    new SelectListItem{Text="2", Value="2"},
                    new SelectListItem{Text="3", Value="3"},
                    new SelectListItem{Text="4", Value="4"}
                };
            }
        }

    }
}