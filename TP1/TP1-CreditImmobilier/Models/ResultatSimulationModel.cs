﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TP1_CreditImmobilier.Models
{
    public class ResultatSimulationModel
    {
        [DisplayName("Simulation")]
        public SimulationModel simulation
        {
            get;
            set;
        }

        [DisplayName("Liste des lignes d'ammortissement")]
        public List<LigneTableauAmortissementModel> listeLignesAmortissement
        {
            get;
            set;
        }

        [DisplayName("Coût total des intérêts")]
        public double coutTotalInteret
        {
            get;
            set;
        }

        [DisplayName("Coût total de l'assurance")]
        public double coutTotalAssurance
        {
            get;
            set;
        }

        [DisplayName("Coût total du prêt")]
        public double coutTotalPret
        {
            get;
            set;
        }

        public ResultatSimulationModel()
        {
            this.listeLignesAmortissement = new List<LigneTableauAmortissementModel>();
        }

        ~ResultatSimulationModel()
        {
            this.listeLignesAmortissement = null;
        }
        /*public void update_amortization_schedule()
        {
            int loan_duration = simulation.settlements_number_per_year * simulation.loan_duration;
            for (int ech = 0; ech < loan_duration; ech++)
            {
                AmortizationSchedule as = new AmortizationSchedule();
                if (ech == 0) {
                    as.CRD = this.;
                } else {

                }
            }
        }*/
    }
}