﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TP1_CreditImmobilier.Models;

namespace TP1_CreditImmobilier.Controllers
{
    public class SimulationModelController : Controller
    {

        private void majResultatSimulation(ResultatSimulationModel resultatSimulation) 
        {
            for (int ech = 0; ech < resultatSimulation.simulation.dureePret; ech++) 
            {
                LigneTableauAmortissementModel la = new LigneTableauAmortissementModel();
                if (ech == 0)
                {
                    la.capitalRestant = resultatSimulation.simulation.capitalEmprunte;
                }
                else
                {
                    LigneTableauAmortissementModel laPrec = resultatSimulation.listeLignesAmortissement[ech - 1];
                    la.capitalRestant = Math.Round(laPrec.capitalRestant - laPrec.amortissement, SimulationModel.nbDecimal);
                }
                la.interet = Math.Round(la.capitalRestant * resultatSimulation.simulation.tauxInteretPeriodique, SimulationModel.nbDecimal);
                resultatSimulation.coutTotalInteret += la.interet;
                if (ech < resultatSimulation.simulation.dureePret - 1)
                {
                    la.amortissement = Math.Round(resultatSimulation.simulation.montantEcheances - la.interet, SimulationModel.nbDecimal);
                }
                else
                {
                    la.amortissement = la.capitalRestant;
                }
                la.frais = Math.Round(resultatSimulation.simulation.montantEcheancesAssurance, SimulationModel.nbDecimal);
                resultatSimulation.coutTotalAssurance += la.frais;
                la.montantReel = la.interet + la.amortissement + la.frais;
                resultatSimulation.coutTotalPret += la.montantReel;
                resultatSimulation.listeLignesAmortissement.Add(la);
            }
        }
        //
        // GET: /Simulation/

        public ActionResult Index()
        {
            return View();
        }
        
        [HttpGet]
        public ActionResult Create()
        {
            SimulationModel s = new SimulationModel();
            s.nomClient = "Pierre de Perpignan";
            s.date = DateTime.Now;
            ViewBag.simulation = s;
            return View(s);
        }

        [HttpPost]
        public ActionResult Create(SimulationModel s)
        {
            if (ModelState.IsValid)
            {
                ResultatSimulationModel simulationResult = new ResultatSimulationModel();
                simulationResult.simulation = s;
                majResultatSimulation(simulationResult);
                return View("~/Views/Shared/Resultat.cshtml", simulationResult);
            }
            else
            {
                return View(s);
            }
        }
    }
}
