﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace TP1_CreditImmobilier.Models
{
    public class SimulationRepository
    {

        private Entities context;

        public SimulationRepository(Entities e)
        {
            context = e;
        }

        public IQueryable<Simulation> All() {
            return  context.Simulations;
        }

        public IQueryable<Simulation> getById(int id)
        {
           return  context.Simulations.Where(s => s.Id == id);
        }

        public IQueryable<Simulation> AllByUser(String user)
        {
            UserProfile up = context.UserProfiles.Where(u => u.UserName == user).First();
            IQueryable<Simulation> simulations =
                context.Simulations.Where(s => s.UserId == up.UserId);
            return simulations;
        }

        public IQueryable<Simulation> AllByUserAboveToTreshold(String user, int capital) 
        {
            UserProfile up = context.UserProfiles.Where(u => u.UserName == user).First();
            IQueryable<Simulation> simulations =
                context.Simulations.Where(s => s.UserId == up.UserId);
            IQueryable<Simulation> simulationsAboveToTreshold = simulations.Where(s => s.Capital > capital);
            return simulationsAboveToTreshold;
        }

        public void Add(Simulation s)
        {
            context.Simulations.Add(s);
        }

        public void Delete(Simulation s)
        {
            var lignes = s.SimulationsLignes.ToList();
            foreach (var l in lignes)
            {
                context.SimulationsLignes.Remove(l);
            }
            context.Simulations.Remove(s);
        }

        public IQueryable<Simulation> Search(String[] items)
        {
            IQueryable<Simulation> simulations = context.Simulations;
            simulations = simulations.Where(s => items.Contains(s.UserProfile.UserName)
                    || items.Contains(s.CoutTotalPret.ToString())
                    || items.Contains(s.Capital.ToString()));
            return simulations;
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}