﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TP1_CreditImmobilier.Models
{
    public class UserRepository
    {
        private Entities context;

        public UserRepository(Entities e)
        {
            context = e;
        }

        public IQueryable<UserProfile> GetUserByUserName(string userName) {
            return context.UserProfiles.Where(u => u.UserName == userName);
        }

        public IQueryable<UserProfile> GetUserById(int id)
        {
            return context.UserProfiles.Where(u => u.UserId == id);
        }
    }
}