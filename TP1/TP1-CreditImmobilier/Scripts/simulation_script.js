﻿function settlements_amount_calcul() {
    var borrowed_capital = parseInt($("#capitalEmprunte").val());
    var loan_duration = parseInt($("#dureePret").val());
    var settlement_number_per_year = parseInt($("#nbEcheanceParAn").val());
    var settlements_amount = borrowed_capital / (loan_duration * settlement_number_per_year);
    return settlements_amount;
}

function periodic_interest_rate_calcul() {
    var annual_interet_rate = parseInt($("#tauxInteretAnnuel").val());
    var settlement_number_per_year = parseInt($("#nbEcheanceParAn").val());
    return annual_interet_rate / settlement_number_per_year;
}

function periodic_insurrance_rate_calcul() {
    var insurrance_rate = parseInt($("#tauxAssurance").val());
    console.log("Insurrance rate = " + insurrance_rate);
    var settlements_number_per_year = parseInt($("#nbEcheanceParAn").val());
    console.log("Settlement number per year = " + settlements_number_per_year);
    var periodic_insurrance_rate = insurrance_rate / settlements_number_per_year;
    console.log("periodic_insurrance rate = " + periodic_insurrance_rate);
    return periodic_insurrance_rate;
}

function insurrance_settlements_amount_calcul() {
    var borrowed_capital = parseInt($("#capitalEmprunte").val());
    console.log("Borrowed capital" + borrowed_capital);
    var periodic_insurrance_rate = periodic_insurrance_rate_calcul();
    console.log("periodic_insurance_rate=" + periodic_insurrance_rate);
    return borrowed_capital / periodic_insurrance_rate;
}

function periodic_interest_rate_handler() {
    var periodic_interest_rate = periodic_interest_rate_calcul();
    $("#tauxInteretPeriodique").text("" + periodic_interest_rate);
}

function periodic_insurance_rate_handler() {
    var periodic_insurance_rate = periodic_insurrance_rate_calcul();
    $("#tauxPeriodiqueAssurance").text("" + periodic_insurance_rate);
}

function insurrance_settlements_handler() {
    console.log("Insurrance settlements handler !!!");
    var insurrance_settlements = insurrance_settlements_amount_calcul();
    $("#montantEcheanceAssurance").text("" + insurrance_settlements);
}


function settlements_amount_handler() {
    var settlements_amount = settlements_amount_calcul();
    $("#montantEcheance").text("" + settlements_amount);
}



$(document).ready(function () {
    // Evenement calcul des echeances du pret
    $("#capitalEmprunte").on('change', settlements_amount_handler);
    $("#dureePret").change(settlements_amount_handler);
    $("#nbEcheanceParAn").change(settlements_amount_handler);

    // Evenement calcul des echeances assurance 
    $("#tauxInteretAnnuel").change(periodic_interest_rate_handler);
    $("#nbEcheanceParAn").change(periodic_interest_rate_handler);

    $("#tauxAssurance").change(periodic_insurance_rate_handler);
    $("#nbEcheanceParAn").change(periodic_insurance_rate_handler);

    $("#capitalEmprunte").on('change', insurrance_settlements_handler);
    $("#tauxAssurance").on('change', insurrance_settlements_handler);
    $("#nbEcheanceParAn").on('change', insurrance_settlements_handler);
});